
import static com.sun.org.apache.xalan.internal.lib.ExsltDatetime.date;
import java.awt.Color;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartFrame;
import org.jfree.chart.JFreeChart;
import org.jfree.data.category.DefaultCategoryDataset;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Andre Erlando
 */
public class MenuUtama extends javax.swing.JFrame {

    /**
     * Creates new form MenuUtama
     */
    Date date = new Date();
    SimpleDateFormat Format=new SimpleDateFormat("yyyy-MM-dd");
    String tanggal = Format.format(date);
    String tipeTransaksi = "pemasukan";
    public MenuUtama() throws SQLException {      
        initComponents();
        tabelHarian.setVisible(false);
        tabelBulanan.setVisible(false);
        tabelTahunan.setVisible(false);
        tabelPencarian.setVisible(false);
        pemasukanCari.setEnabled(false);
        pengeluaranCari.setEnabled(false);
        kategoriCari.setEnabled(false);
        edit.setEnabled(false);
        hapus.setEnabled(false);
        pemasukan.setSelected(true);
        pemasukanCari.setSelected(true);
        tanggal();
        transaksiHarian();
        transaksiBulanan();
        transaksiTahunan();
        fillComboKat();
        fillComboKatCari();
        fillComboRek();
        namaUser();
        
    }
    public void transaksiBerulangHarian() throws SQLException{
        String nama,catatan,frekuensi,tanggal = null;
        int jumlah,idKat,idRek;
        String sqlHarian = "SELECT * FROM transaksi WHERE frekuensi_transaksi = HARIAN";
        PreparedStatement psHarian = new koneksi().getConnection().prepareStatement(sqlHarian);
        ResultSet rsHarian = psHarian.executeQuery(sqlHarian);
        while (rsHarian.next()){
            nama = rsHarian.getString(2);
            jumlah = rsHarian.getInt(3);
            idRek = rsHarian.getInt(4);
            idKat = rsHarian.getInt(5);
            tanggal = rsHarian.getString(6);
            catatan = rsHarian.getString(7);
            frekuensi = rsHarian.getString(8);
        }
        if(this.tanggal.equals(tanggal)){
            
        }
    }
    public String getNamaTransaksi(){
        String nama=null;
        if(transaksiTab.getSelectedIndex() == 0){
                    nama = tabelHarian.getValueAt(tabelHarian.getSelectedRow(),0).toString();
                }
                else if(transaksiTab.getSelectedIndex() == 1){
                    nama = tabelBulanan.getValueAt(tabelBulanan.getSelectedRow(),0).toString();
                }
                else{
                    nama = tabelTahunan.getValueAt(tabelTahunan.getSelectedRow(),0).toString();
                }
        return nama;
    }
    public int getTransaksi(){
        int transaksi;
        if(transaksiTab.getSelectedIndex() == 0){
                    transaksi = Integer.parseInt(tabelHarian.getValueAt(tabelHarian.getSelectedRow(),1).toString());
                }
                else if(transaksiTab.getSelectedIndex() == 1){
                    transaksi = Integer.parseInt(tabelBulanan.getValueAt(tabelBulanan.getSelectedRow(),1).toString());
                }
                else{
                    transaksi = Integer.parseInt(tabelTahunan.getValueAt(tabelTahunan.getSelectedRow(),1).toString());
                }
        return transaksi;
    }
    
    public String getTanggalTransaksi(){
        String tanggalTransaksi=null;
                if(transaksiTab.getSelectedIndex() == 0){
                    tanggalTransaksi = tabelHarian.getValueAt(tabelHarian.getSelectedRow(), 4).toString();
                }
                else if(transaksiTab.getSelectedIndex() == 1){
                    tanggalTransaksi = tabelBulanan.getValueAt(tabelBulanan.getSelectedRow(), 4).toString();
                }
                else{
                    tanggalTransaksi = tabelTahunan.getValueAt(tabelTahunan.getSelectedRow(), 4).toString();
                }
        return tanggalTransaksi;
    }
    public String getCatatan(){
        String catatan=null;
                if(transaksiTab.getSelectedIndex() == 0){
                    catatan = tabelHarian.getValueAt(tabelHarian.getSelectedRow(), 2).toString();
                }
                else if(transaksiTab.getSelectedIndex() == 1){
                    catatan = tabelBulanan.getValueAt(tabelBulanan.getSelectedRow(), 2).toString();
                }
                else{
                    catatan = tabelTahunan.getValueAt(tabelTahunan.getSelectedRow(), 2).toString();
                }
        return catatan;
    }
    public String getFrekuensi(){
        String frekuensi=null;
                if(transaksiTab.getSelectedIndex() == 0){
                    frekuensi = tabelHarian.getValueAt(tabelHarian.getSelectedRow(), 5).toString();
                }
                else if(transaksiTab.getSelectedIndex() == 1){
                    frekuensi = tabelBulanan.getValueAt(tabelBulanan.getSelectedRow(), 5).toString();
                }
                else{
                    frekuensi = tabelTahunan.getValueAt(tabelTahunan.getSelectedRow(), 5).toString();
                }
        return frekuensi;
    }
    int getIdRekening(int id) throws SQLException{
        int idRek = 0;
        String sqlId = "SELECT id_rekening FROM transaksi WHERE id_transaksi ='"+id+"'";
        PreparedStatement ps = new koneksi().getConnection().prepareStatement(sqlId);
        ResultSet rs = ps.executeQuery(sqlId);
            if(rs.next()){
                idRek = rs.getInt(1);
            }
        return idRek;
    }
    int getIdTransaksi(String nama, int jumlah, String tanggal) throws SQLException{
        int id = 0;
        String sqlId = "SELECT id_transaksi FROM transaksi WHERE nama_transaksi ='"+nama+"' AND jumlah_transaksi = "+jumlah+" AND tanggal_transaksi = '"+tanggal+"'";
        PreparedStatement ps = new koneksi().getConnection().prepareStatement(sqlId);
        ResultSet rs = ps.executeQuery(sqlId);
            if(rs.next()){
                id = rs.getInt(1);
            }
        return id;
    }
    public int getIdKategori(int idTransaksi) throws SQLException{
        int idKategori = 0;
        String sqlId = "SELECT id_kategori FROM transaksi WHERE id_transaksi = "+idTransaksi;
        PreparedStatement ps = new koneksi().getConnection().prepareStatement(sqlId);
        ResultSet rs = ps.executeQuery(sqlId);
            if(rs.next()){
                idKategori = rs.getInt(1);
            }
        return idKategori;
    }
    public String getTipeTransaksi(int idKategori) throws SQLException{
        String tipe = null;
        tipe = "SELECT jenis_kategori FROM kategori WHERE id_kategori="+idKategori;
        PreparedStatement ps = new koneksi().getConnection().prepareStatement(tipe);
        ResultSet rs = ps.executeQuery(tipe);
        if(rs.next()){           
               tipe = rs.getString(1); 
        }
         else{
               tipe = "-";
            }
        return tipe;
    }
    
    public void tanggal() throws SQLException{
        tanggalBesar.setText(tanggal);
    }
    public void namaUser() throws SQLException
    {
        String sql = "SELECT nama_user FROM user WHERE 1";
        PreparedStatement ps = new koneksi().getConnection().prepareStatement(sql);
        ResultSet rs = ps.executeQuery(sql);
        if(rs.next()){
            nama.setText("Hallo, "+rs.getString(1));
        }
    }
    public void transaksiHarian() throws SQLException{
        String tipe;
        DefaultTableModel model = new DefaultTableModel(new String[]{"Nama", "Jumlah", "Catatan","tipe","tanggal","Frekuensi"}, 0);
        String sql = "SELECT * FROM transaksi WHERE tanggal_transaksi='"+tanggal+"'";
        PreparedStatement ps = new koneksi().getConnection().prepareStatement(sql);
        ResultSet rs = ps.executeQuery(sql);
            while (rs.next()){
                tipe = getTipeTransaksi(rs.getInt(5));
                String d = rs.getString(2);
                String e = rs.getString(3);
                String f = rs.getString(7);
                String g = tipe;
                String h = rs.getString(6);
                String i = rs.getString(8);
                model.addRow(new Object[]{d, e, f,g,h,i});        
            }
            tabelHarian.setModel(model);
            tabelHarian.setVisible(true);
            
            sql = "SELECT SUM(jumlah_transaksi) FROM transaksi,kategori WHERE tanggal_transaksi = '"+tanggal+"' AND kategori.id_kategori = transaksi.id_kategori AND kategori.jenis_kategori='pengeluaran'";
            ps = new koneksi().getConnection().prepareStatement(sql);
            rs = ps.executeQuery(sql);
            if(rs.next()){
                if(rs.getString(1)==null)
                    pengeluaranHarian.setText("Rp. 0");
                else
                    pengeluaranHarian.setText("Rp. "+rs.getString(1));
            }
            
            sql = "SELECT SUM(jumlah_transaksi) FROM transaksi,kategori WHERE tanggal_transaksi = '"+tanggal+"' AND kategori.id_kategori = transaksi.id_kategori AND kategori.jenis_kategori='pemasukan'";
            ps = new koneksi().getConnection().prepareStatement(sql);
            rs = ps.executeQuery(sql);
            if(rs.next()){
                if(rs.getString(1)==null)
                    pemasukanHarian.setText("Rp. 0");
                else
                    pemasukanHarian.setText("Rp. "+rs.getString(1));
            }
    }
    public void transaksiBulanan() throws SQLException{
        String tipe;
        String bulan,tahun;
        DefaultTableModel model = new DefaultTableModel(new String[]{"Nama", "Jumlah", "Catatan","tipe","tanggal","Frekuensi"}, 0);
        bulan= tanggal.substring(5,7);
        tahun = tanggal.substring(0,4);
        String sql = "SELECT * FROM transaksi WHERE MONTH(tanggal_transaksi)='"+bulan+"' AND YEAR(tanggal_transaksi)='"+tahun+"'";
        PreparedStatement ps = new koneksi().getConnection().prepareStatement(sql);
        ResultSet rs = ps.executeQuery(sql);
            while (rs.next())
            {
                tipe = getTipeTransaksi(rs.getInt(5));
                String d = rs.getString(2);
                String e = rs.getString(3);
                String f = rs.getString(7);
                String g = tipe;
                String h = rs.getString(6);
                String i = rs.getString(8);
                model.addRow(new Object[]{d, e, f,g,h,i});        
    }
            tabelBulanan.setModel(model);
            tabelBulanan.setVisible(true);
            
            sql = "SELECT SUM(jumlah_transaksi) FROM transaksi,kategori WHERE MONTH(tanggal_transaksi)='"+bulan+"' AND YEAR(tanggal_transaksi)='"+tahun+"' AND kategori.id_kategori = transaksi.id_kategori AND kategori.jenis_kategori='pengeluaran'";
            ps = new koneksi().getConnection().prepareStatement(sql);
            rs = ps.executeQuery(sql);
            if(rs.next()){
                if(rs.getString(1)==null)
                    pengeluaranBulanan.setText("Rp. 0");
                else
                    pengeluaranBulanan.setText("Rp. "+rs.getString(1));
            }
            
            sql = "SELECT SUM(jumlah_transaksi) FROM transaksi,kategori WHERE MONTH(tanggal_transaksi)='"+bulan+"' AND YEAR(tanggal_transaksi)='"+tahun+"' AND kategori.id_kategori = transaksi.id_kategori AND kategori.jenis_kategori='pemasukan'";
            ps = new koneksi().getConnection().prepareStatement(sql);
            rs = ps.executeQuery(sql);
            if(rs.next()){
                if(rs.getString(1)==null)
                    pemasukanBulanan.setText("Rp. 0");
                else
                    pemasukanBulanan.setText("Rp. "+rs.getString(1));
            }
    }
    
    public void transaksiTahunan() throws SQLException{
        String tipe;
        String tahun;
        DefaultTableModel model = new DefaultTableModel(new String[]{"Nama", "Jumlah", "Catatan","tipe","tanggal","Frekuensi"}, 0);
        tahun=tanggal.substring(0,4);
        String sql = "SELECT * FROM transaksi WHERE YEAR(tanggal_transaksi)='"+tahun+"'";
        PreparedStatement ps = new koneksi().getConnection().prepareStatement(sql);
        ResultSet rs = ps.executeQuery(sql);
            while (rs.next())
            {
                tipe = getTipeTransaksi(rs.getInt(5));
                String d = rs.getString(2);
                String e = rs.getString(3);
                String f = rs.getString(7);
                String g = tipe;
                String h = rs.getString(6);
                String i = rs.getString(8);
                model.addRow(new Object[]{d, e, f,g,h,i});        
            }
            tabelTahunan.setModel(model);
            tabelTahunan.setVisible(true);
            
            sql = "SELECT SUM(jumlah_transaksi) FROM transaksi,kategori WHERE YEAR(tanggal_transaksi)='"+tahun+"' AND kategori.id_kategori = transaksi.id_kategori AND kategori.jenis_kategori='pengeluaran'";
            ps = new koneksi().getConnection().prepareStatement(sql);
            rs = ps.executeQuery(sql);
            if(rs.next()){
                if(rs.getString(1)==null)
                    pengeluaranTahunan.setText("Rp. 0");
                else
                    pengeluaranTahunan.setText("Rp. "+rs.getString(1));
            }
            
            sql = "SELECT SUM(jumlah_transaksi) FROM transaksi,kategori WHERE YEAR(tanggal_transaksi)='"+tahun+"' AND kategori.id_kategori = transaksi.id_kategori AND kategori.jenis_kategori='pemasukan'";
            ps = new koneksi().getConnection().prepareStatement(sql);
            rs = ps.executeQuery(sql);
            if(rs.next()){
                if(rs.getString(1)==null)
                    pemasukanTahunan.setText("Rp. 0");
                else
                    pemasukanTahunan.setText("Rp. "+rs.getString(1));
            }
    }
    
    public void fillComboKatCari(){
        String comboKat=null;
         try {
             if(pemasukanCari.isSelected()){
                 comboKat = "SELECT * FROM kategori WHERE jenis_kategori='pemasukan'";
             }
             else if(pengeluaranCari.isSelected()){
                 comboKat = "SELECT * FROM kategori WHERE jenis_kategori='pengeluaran'";
             }
             else{
                return;
             }
             
             PreparedStatement psComboKat = new koneksi().getConnection().prepareStatement(comboKat);
             ResultSet rs = psComboKat.executeQuery(comboKat);
            while(rs.next()){
                kategoriCari.addItem(rs.getString(1));
            }
           
        } catch (SQLException ex) {
            Logger.getLogger(tambahTransaksi.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void fillComboKat(){
        String comboKat=null;
         try {
             if(pemasukan.isSelected()){
                 comboKat = "SELECT * FROM kategori WHERE jenis_kategori='pemasukan'";
             }
             else{
                 comboKat = "SELECT * FROM kategori WHERE jenis_kategori='pengeluaran'";
             }
             
             PreparedStatement psComboKat = new koneksi().getConnection().prepareStatement(comboKat);
             ResultSet rs = psComboKat.executeQuery(comboKat);
            while(rs.next()){
                kategori.addItem(rs.getString(1));
            }
        } catch (SQLException ex) {
            Logger.getLogger(tambahTransaksi.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void fillComboRek(){
         try {
            String comboRek = "SELECT * FROM rekening";
             PreparedStatement psComboRek = new koneksi().getConnection().prepareStatement(comboRek);
             ResultSet rs = psComboRek.executeQuery(comboRek);
            while(rs.next()){
                rekening.addItem(rs.getString(1));
                rekeningCari.addItem(rs.getString(1));
            }
        } catch (SQLException ex) {
            Logger.getLogger(tambahTransaksi.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void kurangSaldo(int idRekening,int transaksi) throws SQLException{
           int saldo;
           saldo = new tambahTransaksi().getSaldo(idRekening);
           saldo = saldo - transaksi;
           String sqlRekening = "UPDATE rekening SET saldo = ? WHERE id_rekening ='"+idRekening+"'";
           PreparedStatement psRek = new koneksi().getConnection().prepareStatement(sqlRekening);
           psRek.setInt(1,saldo);
           psRek.executeUpdate();
           psRek.close();
      }
    public void tambahSaldo(int idRekening,int transaksi) throws SQLException{
           int saldo;
           saldo = new tambahTransaksi().getSaldo(idRekening);
           saldo = saldo + transaksi;
           String sqlRekening = "UPDATE rekening SET saldo = ? WHERE id_rekening ='"+idRekening+"'";
           PreparedStatement psRek = new koneksi().getConnection().prepareStatement(sqlRekening);
           psRek.setInt(1,saldo);
           psRek.executeUpdate();
           psRek.close();
      }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup2 = new javax.swing.ButtonGroup();
        jButton2 = new javax.swing.JButton();
        buttonGroup1 = new javax.swing.ButtonGroup();
        jPopupMenu1 = new javax.swing.JPopupMenu();
        jLabel2 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        label2 = new java.awt.Label();
        namaTransaksi = new java.awt.TextField();
        label3 = new java.awt.Label();
        jumlahTransaksi = new java.awt.TextField();
        label4 = new java.awt.Label();
        label5 = new java.awt.Label();
        rekening = new javax.swing.JComboBox<>();
        jButton5 = new javax.swing.JButton();
        pemasukan = new javax.swing.JRadioButton();
        pengeluaran = new javax.swing.JRadioButton();
        kategori = new javax.swing.JComboBox<>();
        label1 = new java.awt.Label();
        transaksiTab = new javax.swing.JTabbedPane();
        harian = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tabelHarian = new javax.swing.JTable();
        jLabel4 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        pemasukanHarian = new javax.swing.JLabel();
        pengeluaranHarian = new javax.swing.JLabel();
        bulanan = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tabelBulanan = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        pemasukanBulanan = new javax.swing.JLabel();
        pengeluaranBulanan = new javax.swing.JLabel();
        tahunan = new javax.swing.JPanel();
        jScrollPane4 = new javax.swing.JScrollPane();
        tabelTahunan = new javax.swing.JTable();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        pemasukanTahunan = new javax.swing.JLabel();
        pengeluaranTahunan = new javax.swing.JLabel();
        pencarian = new javax.swing.JPanel();
        jButton1 = new javax.swing.JButton();
        label9 = new java.awt.Label();
        nama_transaksi = new java.awt.TextField();
        label10 = new java.awt.Label();
        label11 = new java.awt.Label();
        pemasukanCari = new javax.swing.JRadioButton();
        pengeluaranCari = new javax.swing.JRadioButton();
        kategoriCari = new javax.swing.JComboBox<>();
        rekeningCari = new javax.swing.JComboBox<>();
        jPanel8 = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        tabelPencarian = new javax.swing.JTable();
        cekKat = new javax.swing.JCheckBox();
        edit = new javax.swing.JButton();
        hapus = new javax.swing.JButton();
        jButton9 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jButton8 = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        label6 = new java.awt.Label();
        rekening1 = new java.awt.Label();
        saldo1 = new java.awt.Label();
        jButton10 = new javax.swing.JButton();
        jButton7 = new javax.swing.JButton();
        tanggalBesar = new javax.swing.JLabel();
        nama = new javax.swing.JLabel();

        jButton2.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        jButton2.setText("SELESAI");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });

        jPanel1.setBackground(new java.awt.Color(61, 185, 162));

        jPanel2.setBackground(new java.awt.Color(154, 154, 153));

        label2.setAlignment(java.awt.Label.CENTER);
        label2.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        label2.setForeground(new java.awt.Color(255, 255, 255));
        label2.setText("NAMA:");

        namaTransaksi.setFont(new java.awt.Font("Calibri", 0, 14)); // NOI18N

        label3.setAlignment(java.awt.Label.CENTER);
        label3.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        label3.setForeground(new java.awt.Color(255, 255, 255));
        label3.setText("JUMLAH:");

        jumlahTransaksi.setFont(new java.awt.Font("Calibri", 0, 14)); // NOI18N

        label4.setAlignment(java.awt.Label.CENTER);
        label4.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        label4.setForeground(new java.awt.Color(255, 255, 255));
        label4.setText("KATEGORI:");

        label5.setAlignment(java.awt.Label.CENTER);
        label5.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        label5.setForeground(new java.awt.Color(255, 255, 255));
        label5.setText("REKENING:");

        rekening.setFont(new java.awt.Font("Calibri", 1, 12)); // NOI18N
        rekening.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rekeningActionPerformed(evt);
            }
        });

        jButton5.setFont(new java.awt.Font("Calibri", 1, 18)); // NOI18N
        jButton5.setText("SIMPAN");
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });

        buttonGroup2.add(pemasukan);
        pemasukan.setForeground(new java.awt.Color(255, 255, 255));
        pemasukan.setText("PEMASUKAN");
        pemasukan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pemasukanActionPerformed(evt);
            }
        });

        buttonGroup2.add(pengeluaran);
        pengeluaran.setForeground(new java.awt.Color(255, 255, 255));
        pengeluaran.setText("PENGELUARAN");
        pengeluaran.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pengeluaranActionPerformed(evt);
            }
        });

        kategori.setFont(new java.awt.Font("Calibri", 1, 12)); // NOI18N
        kategori.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                kategoriActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(label2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(32, 32, 32)
                        .addComponent(namaTransaksi, javax.swing.GroupLayout.PREFERRED_SIZE, 248, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(label4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jButton5, javax.swing.GroupLayout.PREFERRED_SIZE, 146, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(kategori, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                                .addComponent(pemasukan)
                                .addGap(18, 18, 18)
                                .addComponent(pengeluaran)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(label5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(label3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jumlahTransaksi, javax.swing.GroupLayout.PREFERRED_SIZE, 147, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(rekening, javax.swing.GroupLayout.PREFERRED_SIZE, 146, javax.swing.GroupLayout.PREFERRED_SIZE))))))
                .addGap(32, 32, 32))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(namaTransaksi, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(label2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(label3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jumlahTransaksi, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(pemasukan)
                            .addComponent(pengeluaran))
                        .addComponent(label4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(label5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(rekening, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(13, 13, 13)
                .addComponent(kategori, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton5)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        label1.setAlignment(java.awt.Label.CENTER);
        label1.setFont(new java.awt.Font("Calibri", 1, 24)); // NOI18N
        label1.setForeground(new java.awt.Color(255, 255, 255));
        label1.setText("TRANSAKSI CEPAT");

        transaksiTab.setFont(new java.awt.Font("Calibri", 1, 12)); // NOI18N
        transaksiTab.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                transaksiTabStateChanged(evt);
            }
        });

        tabelHarian.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null}
            },
            new String [] {
                "Nama transaksi", "Jumlah", "Catatan", "Tipe", "Tanggal", "Frekuensi"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.Object.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tabelHarian.setToolTipText("");
        tabelHarian.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tabelHarianMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tabelHarian);

        jLabel4.setFont(new java.awt.Font("Calibri", 1, 12)); // NOI18N
        jLabel4.setText("Total Pemasukan : ");

        jLabel6.setFont(new java.awt.Font("Calibri", 1, 12)); // NOI18N
        jLabel6.setText("Total Pengeluaran : ");

        pemasukanHarian.setFont(new java.awt.Font("Calibri", 1, 12)); // NOI18N
        pemasukanHarian.setText("jLabel7");

        pengeluaranHarian.setFont(new java.awt.Font("Calibri", 1, 12)); // NOI18N
        pengeluaranHarian.setText("Total Pengeluaran : ");

        javax.swing.GroupLayout harianLayout = new javax.swing.GroupLayout(harian);
        harian.setLayout(harianLayout);
        harianLayout.setHorizontalGroup(
            harianLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(harianLayout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 594, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, harianLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(harianLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel4)
                    .addComponent(jLabel6))
                .addGap(28, 28, 28)
                .addGroup(harianLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(pemasukanHarian, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(pengeluaranHarian, javax.swing.GroupLayout.PREFERRED_SIZE, 143, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(65, 65, 65))
        );
        harianLayout.setVerticalGroup(
            harianLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, harianLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(harianLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(pemasukanHarian))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(harianLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(pengeluaranHarian))
                .addGap(281, 281, 281))
        );

        transaksiTab.addTab("HARIAN", harian);

        tabelBulanan.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null}
            },
            new String [] {
                "Nama transaksi", "Jumlah", "Catatan", "Tipe", "Tanggal", "Frekuensi"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.Object.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tabelBulanan.setToolTipText("");
        tabelBulanan.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tabelBulananMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(tabelBulanan);

        jLabel1.setFont(new java.awt.Font("Calibri", 1, 12)); // NOI18N
        jLabel1.setText("Total Pemasukan : ");

        jLabel3.setFont(new java.awt.Font("Calibri", 1, 12)); // NOI18N
        jLabel3.setText("Total Pengeluaran : ");

        pemasukanBulanan.setFont(new java.awt.Font("Calibri", 1, 12)); // NOI18N
        pemasukanBulanan.setText("jLabel4");

        pengeluaranBulanan.setFont(new java.awt.Font("Calibri", 1, 12)); // NOI18N
        pengeluaranBulanan.setText("jLabel4");

        javax.swing.GroupLayout bulananLayout = new javax.swing.GroupLayout(bulanan);
        bulanan.setLayout(bulananLayout);
        bulananLayout.setHorizontalGroup(
            bulananLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(bulananLayout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addGroup(bulananLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(bulananLayout.createSequentialGroup()
                        .addGap(317, 317, 317)
                        .addGroup(bulananLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(bulananLayout.createSequentialGroup()
                                .addComponent(jLabel1)
                                .addGap(18, 18, 18)
                                .addComponent(pemasukanBulanan, javax.swing.GroupLayout.PREFERRED_SIZE, 153, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(bulananLayout.createSequentialGroup()
                                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(pengeluaranBulanan, javax.swing.GroupLayout.PREFERRED_SIZE, 153, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 594, Short.MAX_VALUE))
                .addContainerGap())
        );
        bulananLayout.setVerticalGroup(
            bulananLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, bulananLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(bulananLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(pemasukanBulanan)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(bulananLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(pengeluaranBulanan)
                    .addComponent(jLabel3))
                .addGap(265, 265, 265))
        );

        transaksiTab.addTab("BULANAN", bulanan);

        tabelTahunan.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null}
            },
            new String [] {
                "Nama transaksi", "Jumlah", "Catatan", "Tipe", "Tanggal", "Frekuensi"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.Object.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tabelTahunan.setToolTipText("");
        tabelTahunan.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tabelTahunanMouseClicked(evt);
            }
        });
        jScrollPane4.setViewportView(tabelTahunan);

        jLabel7.setFont(new java.awt.Font("Calibri", 1, 12)); // NOI18N
        jLabel7.setText("Total Pemasukan : ");

        jLabel8.setFont(new java.awt.Font("Calibri", 1, 12)); // NOI18N
        jLabel8.setText("Total Pengeluaran : ");

        pemasukanTahunan.setFont(new java.awt.Font("Calibri", 1, 12)); // NOI18N
        pemasukanTahunan.setText("jLabel9");

        pengeluaranTahunan.setFont(new java.awt.Font("Calibri", 1, 12)); // NOI18N
        pengeluaranTahunan.setText("Total Pengeluaran : ");

        javax.swing.GroupLayout tahunanLayout = new javax.swing.GroupLayout(tahunan);
        tahunan.setLayout(tahunanLayout);
        tahunanLayout.setHorizontalGroup(
            tahunanLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(tahunanLayout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 594, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, tahunanLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(tahunanLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel7)
                    .addComponent(jLabel8))
                .addGap(37, 37, 37)
                .addGroup(tahunanLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(pemasukanTahunan, javax.swing.GroupLayout.PREFERRED_SIZE, 144, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(pengeluaranTahunan))
                .addGap(56, 56, 56))
        );
        tahunanLayout.setVerticalGroup(
            tahunanLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, tahunanLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(tahunanLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(pemasukanTahunan)
                    .addComponent(jLabel7))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(tahunanLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(pengeluaranTahunan)
                    .addComponent(jLabel8))
                .addGap(271, 271, 271))
        );

        transaksiTab.addTab("TAHUNAN", tahunan);

        jButton1.setFont(new java.awt.Font("Calibri", 1, 12)); // NOI18N
        jButton1.setText("CARI");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        label9.setAlignment(java.awt.Label.CENTER);
        label9.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        label9.setForeground(new java.awt.Color(0, 0, 0));
        label9.setText("Nama:");

        nama_transaksi.setFont(new java.awt.Font("Calibri", 0, 14)); // NOI18N

        label10.setAlignment(java.awt.Label.CENTER);
        label10.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        label10.setForeground(new java.awt.Color(0, 0, 0));
        label10.setText("Kategori:");

        label11.setAlignment(java.awt.Label.CENTER);
        label11.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        label11.setForeground(new java.awt.Color(0, 0, 0));
        label11.setText("Rekening:");

        buttonGroup1.add(pemasukanCari);
        pemasukanCari.setText("PEMASUKAN");
        pemasukanCari.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pemasukanCariActionPerformed(evt);
            }
        });

        buttonGroup1.add(pengeluaranCari);
        pengeluaranCari.setText("PENGELUARAN");
        pengeluaranCari.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pengeluaranCariActionPerformed(evt);
            }
        });

        kategoriCari.setFont(new java.awt.Font("Calibri", 1, 12)); // NOI18N
        kategoriCari.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                kategoriCariActionPerformed(evt);
            }
        });

        rekeningCari.setFont(new java.awt.Font("Calibri", 1, 12)); // NOI18N
        rekeningCari.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rekeningCariActionPerformed(evt);
            }
        });

        tabelPencarian.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "No", "Nama", "Tanggal", "Jumlah"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        tabelPencarian.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tabelPencarianMouseClicked(evt);
            }
        });
        jScrollPane3.setViewportView(tabelPencarian);

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 329, Short.MAX_VALUE)
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 174, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        cekKat.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cekKatActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pencarianLayout = new javax.swing.GroupLayout(pencarian);
        pencarian.setLayout(pencarianLayout);
        pencarianLayout.setHorizontalGroup(
            pencarianLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pencarianLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pencarianLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pencarianLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(pencarianLayout.createSequentialGroup()
                            .addGroup(pencarianLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(label10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(label9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(pencarianLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(pencarianLayout.createSequentialGroup()
                                    .addGap(8, 8, 8)
                                    .addComponent(cekKat)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                    .addComponent(pemasukanCari)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                    .addComponent(pengeluaranCari))
                                .addGroup(pencarianLayout.createSequentialGroup()
                                    .addGap(15, 15, 15)
                                    .addComponent(nama_transaksi, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pencarianLayout.createSequentialGroup()
                            .addComponent(label11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(14, 14, 14)
                            .addGroup(pencarianLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(rekeningCari, 0, 196, Short.MAX_VALUE)
                                .addComponent(kategoriCari, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                    .addComponent(jButton1, javax.swing.GroupLayout.Alignment.TRAILING))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        pencarianLayout.setVerticalGroup(
            pencarianLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pencarianLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pencarianLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(pencarianLayout.createSequentialGroup()
                        .addGroup(pencarianLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(nama_transaksi, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(label9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pencarianLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(pencarianLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(pemasukanCari)
                                .addComponent(pengeluaranCari))
                            .addComponent(cekKat)
                            .addComponent(label10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(kategoriCari, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pencarianLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(label11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(rekeningCari, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton1)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );

        transaksiTab.addTab("PENCARIAN", pencarian);

        edit.setText("Edit");
        edit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                editActionPerformed(evt);
            }
        });

        hapus.setText("Hapus");
        hapus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                hapusActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(label1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
            .addComponent(transaksiTab, javax.swing.GroupLayout.Alignment.TRAILING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(edit)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(hapus)
                .addGap(35, 35, 35))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(label1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(transaksiTab, javax.swing.GroupLayout.PREFERRED_SIZE, 216, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(hapus)
                    .addComponent(edit))
                .addContainerGap())
        );

        jButton9.setFont(new java.awt.Font("Calibri", 1, 12)); // NOI18N
        jButton9.setText("TAMBAH TRANSAKSI");
        jButton9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton9ActionPerformed(evt);
            }
        });

        jButton4.setFont(new java.awt.Font("Calibri", 1, 12)); // NOI18N
        jButton4.setText("KATEGORI");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });

        jButton8.setFont(new java.awt.Font("Calibri", 1, 12)); // NOI18N
        jButton8.setText("REPORT");
        jButton8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton8ActionPerformed(evt);
            }
        });

        jPanel3.setBackground(new java.awt.Color(61, 185, 162));

        label6.setAlignment(java.awt.Label.CENTER);
        label6.setFont(new java.awt.Font("Calibri", 1, 18)); // NOI18N
        label6.setForeground(new java.awt.Color(255, 255, 255));
        label6.setText("REKENING");

        rekening1.setAlignment(java.awt.Label.CENTER);
        rekening1.setFont(new java.awt.Font("Calibri", 1, 12)); // NOI18N
        rekening1.setForeground(new java.awt.Color(255, 255, 255));
        rekening1.setText("Dompet");

        saldo1.setAlignment(java.awt.Label.CENTER);
        saldo1.setFont(new java.awt.Font("Calibri", 1, 12)); // NOI18N
        saldo1.setForeground(new java.awt.Color(255, 255, 255));
        saldo1.setText("Rp.1000000");

        jButton10.setFont(new java.awt.Font("Calibri", 1, 12)); // NOI18N
        jButton10.setText("ATUR REKENING");
        jButton10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton10ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButton10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(label6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addComponent(rekening1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(48, 48, 48)
                                .addComponent(saldo1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(label6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(rekening1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(saldo1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 91, Short.MAX_VALUE)
                .addComponent(jButton10)
                .addContainerGap())
        );

        jButton7.setFont(new java.awt.Font("Calibri", 1, 12)); // NOI18N
        jButton7.setForeground(new java.awt.Color(255, 0, 0));
        jButton7.setText("QUIT");
        jButton7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton7ActionPerformed(evt);
            }
        });

        tanggalBesar.setBackground(new java.awt.Color(0, 0, 0));
        tanggalBesar.setFont(new java.awt.Font("Calibri", 1, 18)); // NOI18N
        tanggalBesar.setText("OKTOBER");
        tanggalBesar.setMaximumSize(new java.awt.Dimension(50, 50));

        nama.setFont(new java.awt.Font("Calibri", 3, 24)); // NOI18N
        nama.setText("jLabel9");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(nama, javax.swing.GroupLayout.PREFERRED_SIZE, 281, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(386, 386, 386)
                        .addComponent(tanggalBesar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jButton4, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jPanel3, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jButton9, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jButton8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jButton7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(16, 16, 16)
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 353, Short.MAX_VALUE)
                        .addComponent(jLabel2)
                        .addGap(72, 72, 72))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(24, Short.MAX_VALUE)
                .addComponent(jLabel2)
                .addGap(576, 576, 576))
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(nama, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 17, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(tanggalBesar, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(26, 26, 26)
                        .addComponent(jButton9, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jButton4)
                        .addGap(25, 25, 25)
                        .addComponent(jButton8)
                        .addGap(42, 42, 42)
                        .addComponent(jButton7))
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(64, 64, 64))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened
        // TODO add your handling code here:
        String sql = "SELECT * FROM rekening WHERE 1";
        try {
            PreparedStatement ps = new koneksi().getConnection().prepareStatement(sql);
            ResultSet rs=ps.executeQuery(sql);
            while(rs.next()){
                    rekening1.setText(rs.getString(1));
                    saldo1.setText("Rp."+rs.getString(2));                  
            }
        } catch (SQLException ex) {
            Logger.getLogger(MenuUtama.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
    }//GEN-LAST:event_formWindowOpened

    private void jButton7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton7ActionPerformed
        // TODO add your handling code here:
         int confirmed = JOptionPane.showConfirmDialog(null, 
        "Apakah anda benar-benar ingin keluar?", "Exit Program Message Box",
        JOptionPane.YES_NO_OPTION);
        if (confirmed == JOptionPane.YES_OPTION) {
      System.exit(0);
    }
    }//GEN-LAST:event_jButton7ActionPerformed

    private void rekeningActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rekeningActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_rekeningActionPerformed

    private void pemasukanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pemasukanActionPerformed
        // TODO add your handling code here:
        kategori.removeAllItems();
        fillComboKat();
        tipeTransaksi = "pemasukan";
    }//GEN-LAST:event_pemasukanActionPerformed

    private void kategoriActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_kategoriActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_kategoriActionPerformed

    private void jButton9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton9ActionPerformed
        new tambahTransaksi().setVisible(true);
        this.setVisible(false);
// TODO add your handling code here:
    }//GEN-LAST:event_jButton9ActionPerformed

    private void jButton10ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton10ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton10ActionPerformed

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
        try{
           int idRekening;
           if(namaTransaksi.getText().equals("")){
           JOptionPane.showMessageDialog(null, "Nama Tidak Boleh Kosong");
            }
           int transaksi = Integer.parseInt(jumlahTransaksi.getText());
           if(transaksi<0){
               JOptionPane.showMessageDialog(null, "Jumlah Transaksi Tidak Boleh Negatif");
           }
           else{
           String sql = "INSERT INTO transaksi " +
                   "VALUES (NULL ,?, ?, ?,?,?,'-','-')";
           PreparedStatement ps = new koneksi().getConnection().prepareStatement(sql);
           ps.setString(1, namaTransaksi.getText());
           ps.setInt(2, transaksi);
           
           idRekening = new tambahTransaksi().rekening((String) rekening.getSelectedItem());
           
           ps.setInt(3,idRekening);
           ps.setInt(4, new tambahTransaksi().kategori((String) kategori.getSelectedItem())); 
           ps.setString(5, tanggal);    
           ps.executeUpdate();
           ps.close();
           if(tipeTransaksi=="pemasukan"){
               tambahSaldo(idRekening,transaksi);
           }
           else{
              kurangSaldo(idRekening,transaksi); 
           }
           new MenuUtama().setVisible(true);
           this.setVisible(false);
           }
        }catch(NumberFormatException nfe){
            JOptionPane.showMessageDialog(null, "Invalid input");
        }
        catch(SQLException se){
      //Handle errors for JDBC
            se.printStackTrace();
        }catch(Exception e){
      //Handle errors for Class.forName
            e.printStackTrace();
        }
    }//GEN-LAST:event_jButton5ActionPerformed

    private void pengeluaranActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pengeluaranActionPerformed
        // TODO add your handling code here:
        kategori.removeAllItems();
        fillComboKat();
        tipeTransaksi = "pengeluaran";
        
    }//GEN-LAST:event_pengeluaranActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        // TODO add your handling code here:
        this.setVisible(false);
        new kategori().setVisible(true);
    }//GEN-LAST:event_jButton4ActionPerformed

    private void rekeningCariActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rekeningCariActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_rekeningCariActionPerformed

    private void kategoriCariActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_kategoriCariActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_kategoriCariActionPerformed

    private void pemasukanCariActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pemasukanCariActionPerformed
        // TODO add your handling code here:
        kategoriCari.removeAllItems();
        fillComboKatCari();
    }//GEN-LAST:event_pemasukanCariActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        try {                                         
            // TODO add your handling code here:
            int idKategori = new tambahTransaksi().kategori((String) kategoriCari.getSelectedItem());
            int idRekening = new tambahTransaksi().rekening((String) rekening.getSelectedItem());
            DefaultTableModel model = new DefaultTableModel(new String[]{"No","Nama","tanggal","Jumlah"}, 0);
            int no=1;
            String sql=null;
            if(cekKat.isSelected()){
                if(nama_transaksi.getText().equals("")){                    
                    sql = "SELECT nama_transaksi,tanggal_transaksi,jumlah_transaksi FROM transaksi WHERE id_kategori ="+idKategori+" AND id_rekening ="+idRekening;
                }
                else{
                    sql = "SELECT nama_transaksi,tanggal_transaksi,jumlah_transaksi FROM transaksi WHERE nama_transaksi ='"+nama_transaksi.getText()+"' AND id_kategori ="+idKategori+" AND id_rekening ="+idRekening;
                }
                
            }
            else if(!nama_transaksi.getText().equals("")){               
                sql = "SELECT nama_transaksi,tanggal_transaksi,jumlah_transaksi FROM transaksi WHERE nama_transaksi ='"+nama_transaksi.getText()+"' AND id_rekening ="+idRekening;
            }
            else{
                sql = "SELECT nama_transaksi,tanggal_transaksi,jumlah_transaksi FROM transaksi WHERE id_rekening ="+idRekening;
            }
            try {
                PreparedStatement ps = new koneksi().getConnection().prepareStatement(sql);
                ResultSet rs = ps.executeQuery(sql);
                while(rs.next()){
                    int d = no;
                    String e = rs.getString(1);
                    String f = rs.getString(2);
                    String g = rs.getString(3);
                    model.addRow(new Object[]{d,e,f,g});
                    no++;
                }
                tabelPencarian.setModel(model);
                tabelPencarian.setVisible(true);
            } catch (SQLException ex) {
                Logger.getLogger(MenuUtama.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(MenuUtama.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }//GEN-LAST:event_jButton1ActionPerformed

    private void pengeluaranCariActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pengeluaranCariActionPerformed
        // TODO add your handling code here:
        kategoriCari.removeAllItems();
        fillComboKatCari();
    }//GEN-LAST:event_pengeluaranCariActionPerformed

    private void cekKatActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cekKatActionPerformed
        // TODO add your handling code here:
        if(cekKat.isSelected()){
            pemasukanCari.setEnabled(true);
            pengeluaranCari.setEnabled(true);
            kategoriCari.setEnabled(true);
        }
        else{
            pemasukanCari.setEnabled(false);
            pengeluaranCari.setEnabled(false);
            kategoriCari.setEnabled(false);
        }
    }//GEN-LAST:event_cekKatActionPerformed

    private void editActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_editActionPerformed
        try {
            // TODO add your handling code here:
            String namaTransaksi = getNamaTransaksi();
            String frekuensi = getFrekuensi();
            int transaksi = getTransaksi();
            String tanggal = getTanggalTransaksi();
            int idTransaksi = getIdTransaksi(namaTransaksi,transaksi,tanggal);
            //System.out.println(idTransaksi);
            int idRekening = getIdRekening(idTransaksi);
            //System.out.println(idRekening);
            int idKategori = getIdKategori(idTransaksi);
            String catatan = getCatatan();
            this.setVisible(false);
            EditTransaksi edit = new EditTransaksi();
            edit.setData(namaTransaksi,transaksi,idRekening,getTipeTransaksi(idKategori),idKategori,tanggal,catatan,frekuensi);
            edit.setIdTransaksi(getIdTransaksi(namaTransaksi,transaksi,tanggal));
            edit.cekFrekuensi();
            edit.setVisible(true);
            
        } catch (SQLException ex) {
            Logger.getLogger(MenuUtama.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(MenuUtama.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_editActionPerformed

    private void tabelBulananMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tabelBulananMouseClicked
        // TODO add your handling code here:
        edit.setEnabled(true);
        hapus.setEnabled(true);
    }//GEN-LAST:event_tabelBulananMouseClicked

    private void tabelTahunanMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tabelTahunanMouseClicked
        // TODO add your handling code here:
        edit.setEnabled(true);
        hapus.setEnabled(true);
    }//GEN-LAST:event_tabelTahunanMouseClicked

    private void tabelHarianMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tabelHarianMouseClicked
        // TODO add your handling code here:
        edit.setEnabled(true);
        hapus.setEnabled(true);
    }//GEN-LAST:event_tabelHarianMouseClicked

    private void transaksiTabStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_transaksiTabStateChanged
        // TODO add your handling code here:
        edit.setEnabled(false);
        hapus.setEnabled(false);
    }//GEN-LAST:event_transaksiTabStateChanged

    private void hapusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_hapusActionPerformed
        // TODO add your handling code here:
        int idTransaksi;
        int idRekening;
        int idKategori;
        String namaTransaksi = null;
        int transaksi;
        String tanggal;
        int confirmed = JOptionPane.showConfirmDialog(null, 
        "Transaksi ini akan anda HAPUS?", "Exit Program Message Box",
        JOptionPane.YES_NO_OPTION);
        if (confirmed == JOptionPane.YES_OPTION) {      
            try {
                namaTransaksi = getNamaTransaksi();
                transaksi = getTransaksi();
                tanggal = getTanggalTransaksi();
                idTransaksi = getIdTransaksi(namaTransaksi,transaksi,tanggal);
                //System.out.println(idTransaksi);
                idRekening = getIdRekening(idTransaksi);
                //System.out.println(idRekening);
                idKategori = getIdKategori(idTransaksi);
                //System.out.println(getTipeTransaksi(idKategori));
                if(getTipeTransaksi(idKategori).equals("pengeluaran")){
                    tambahSaldo(idRekening,transaksi);
                    //System.out.println("GK MAU");
                    
                }
                else{
                    kurangSaldo(idRekening,transaksi); 
                }
                String sql = "DELETE FROM transaksi WHERE id_transaksi = "+idTransaksi;
                PreparedStatement ps = new koneksi().getConnection().prepareStatement(sql);
                ps.executeUpdate();
                new MenuUtama().setVisible(true);
                this.setVisible(false);
            } catch (SQLException ex) {
                Logger.getLogger(MenuUtama.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }//GEN-LAST:event_hapusActionPerformed

    private void jButton8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton8ActionPerformed
        // TODO add your handling code here:
         try {
            // TODO add your handling code here:
            int pemasukanJan = 0;
            String sql1 = "SELECT SUM(jumlah_transaksi) FROM transaksi,kategori WHERE transaksi.id_kategori = kategori.id_kategori AND jenis_kategori = 'Pemasukan' AND MONTH(tanggal_transaksi) = 1";
            PreparedStatement ps1 = new koneksi().getConnection().prepareStatement(sql1);
            ResultSet rs1 = ps1.executeQuery(sql1);
            if(rs1.next()){
               pemasukanJan = rs1.getInt(1);
            }
                   
            
            int pemasukanFeb = 0;
            String sql2 = "SELECT SUM(jumlah_transaksi) FROM transaksi,kategori WHERE transaksi.id_kategori = kategori.id_kategori AND jenis_kategori = 'Pemasukan' AND MONTH(tanggal_transaksi) = 2";
            PreparedStatement ps2 = new koneksi().getConnection().prepareStatement(sql2);
            ResultSet rs2 = ps2.executeQuery(sql2);
            if(rs2.next()){
               pemasukanFeb = rs2.getInt(1);
            }
            
            int pemasukanMar = 0;
            String sql3 = "SELECT SUM(jumlah_transaksi) FROM transaksi,kategori WHERE transaksi.id_kategori = kategori.id_kategori AND jenis_kategori = 'Pemasukan' AND MONTH(tanggal_transaksi) = 3";
            PreparedStatement ps3 = new koneksi().getConnection().prepareStatement(sql3);
            ResultSet rs3 = ps3.executeQuery(sql3);
            if(rs3.next()){
               pemasukanFeb = rs3.getInt(1);
            }
            
            int pemasukanApr = 0;
            String sql4 = "SELECT SUM(jumlah_transaksi) FROM transaksi,kategori WHERE transaksi.id_kategori = kategori.id_kategori AND jenis_kategori = 'Pemasukan' AND MONTH(tanggal_transaksi) = 4";
            PreparedStatement ps4 = new koneksi().getConnection().prepareStatement(sql4);
            ResultSet rs4 = ps4.executeQuery(sql4);
            if(rs4.next()){
               pemasukanApr = rs4.getInt(1);
            }
            
            int pemasukanMei = 0;
            String sql5 = "SELECT SUM(jumlah_transaksi) FROM transaksi,kategori WHERE transaksi.id_kategori = kategori.id_kategori AND jenis_kategori = 'Pemasukan' AND MONTH(tanggal_transaksi) = 5";
            PreparedStatement ps5 = new koneksi().getConnection().prepareStatement(sql5);
            ResultSet rs5 = ps5.executeQuery(sql5);
            if(rs5.next()){
               pemasukanMei = rs5.getInt(1);
            }
            
            int pemasukanJun = 0;
            String sql6 = "SELECT SUM(jumlah_transaksi) FROM transaksi,kategori WHERE transaksi.id_kategori = kategori.id_kategori AND jenis_kategori = 'Pemasukan' AND MONTH(tanggal_transaksi) = 6";
            PreparedStatement ps6 = new koneksi().getConnection().prepareStatement(sql6);
            ResultSet rs6 = ps6.executeQuery(sql6);
            if(rs6.next()){
               pemasukanJun = rs6.getInt(1);
            }
            
            int pemasukanJul = 0;
            String sql7 = "SELECT SUM(jumlah_transaksi) FROM transaksi,kategori WHERE transaksi.id_kategori = kategori.id_kategori AND jenis_kategori = 'Pemasukan' AND MONTH(tanggal_transaksi) = 7";
            PreparedStatement ps7 = new koneksi().getConnection().prepareStatement(sql7);
            ResultSet rs7 = ps7.executeQuery(sql7);
            if(rs7.next()){
               pemasukanJul = rs7.getInt(1);
            }
            
            int pemasukanAgu = 0;
            String sql8 = "SELECT SUM(jumlah_transaksi) FROM transaksi,kategori WHERE transaksi.id_kategori = kategori.id_kategori AND jenis_kategori = 'Pemasukan' AND MONTH(tanggal_transaksi) = 8";
            PreparedStatement ps8 = new koneksi().getConnection().prepareStatement(sql8);
            ResultSet rs8 = ps8.executeQuery(sql8);
            if(rs8.next()){
               pemasukanAgu = rs8.getInt(1);
            }
            
            int pemasukanSep = 0;
            String sql9 = "SELECT SUM(jumlah_transaksi) FROM transaksi,kategori WHERE transaksi.id_kategori = kategori.id_kategori AND jenis_kategori = 'Pemasukan' AND MONTH(tanggal_transaksi) = 9";
            PreparedStatement ps9 = new koneksi().getConnection().prepareStatement(sql9);
            ResultSet rs9 = ps5.executeQuery(sql9);
            if(rs9.next()){
               pemasukanSep = rs9.getInt(1);
            }
            
            int pemasukanOkt = 0;
            String sql10 = "SELECT SUM(jumlah_transaksi) FROM transaksi,kategori WHERE transaksi.id_kategori = kategori.id_kategori AND jenis_kategori = 'Pemasukan' AND MONTH(tanggal_transaksi) = 10";
            PreparedStatement ps10 = new koneksi().getConnection().prepareStatement(sql10);
            ResultSet rs10 = ps10.executeQuery(sql10);
            if(rs10.next()){
               pemasukanOkt = rs10.getInt(1);
            }
            
            int pemasukanNov = 0;
            String sql11 = "SELECT SUM(jumlah_transaksi) FROM transaksi,kategori WHERE transaksi.id_kategori = kategori.id_kategori AND jenis_kategori = 'Pemasukan' AND MONTH(tanggal_transaksi) = 11";
            PreparedStatement ps11 = new koneksi().getConnection().prepareStatement(sql11);
            ResultSet rs11 = ps11.executeQuery(sql11);
            if(rs11.next()){
               pemasukanNov = rs11.getInt(1);
            }
            
            int pemasukanDes = 0;
            String sql12 = "SELECT SUM(jumlah_transaksi) FROM transaksi,kategori WHERE transaksi.id_kategori = kategori.id_kategori AND jenis_kategori = 'Pemasukan' AND MONTH(tanggal_transaksi) = 12";
            PreparedStatement ps12 = new koneksi().getConnection().prepareStatement(sql12);
            ResultSet rs12 = ps12.executeQuery(sql12);
            if(rs12.next()){
               pemasukanDes = rs12.getInt(1);
            }
            
            int pengeluaranJan = 0;
            String sql13 = "SELECT SUM(jumlah_transaksi) FROM transaksi,kategori WHERE transaksi.id_kategori = kategori.id_kategori AND jenis_kategori = 'Pengeluaran' AND MONTH(tanggal_transaksi) = 1";
            PreparedStatement ps13 = new koneksi().getConnection().prepareStatement(sql13);
            ResultSet rs13 = ps13.executeQuery(sql13);
            if(rs13.next()){
               pengeluaranJan = rs13.getInt(1);
            }
                   
            
            int pengeluaranFeb = 0;
            String sql14 = "SELECT SUM(jumlah_transaksi) FROM transaksi,kategori WHERE transaksi.id_kategori = kategori.id_kategori AND jenis_kategori = 'Pengeluaran' AND MONTH(tanggal_transaksi) = 2";
            PreparedStatement ps14 = new koneksi().getConnection().prepareStatement(sql14);
            ResultSet rs14 = ps14.executeQuery(sql14);
            if(rs14.next()){
               pengeluaranFeb = rs14.getInt(1);
            }
            
            int pengeluaranMar = 0;
            String sql15 = "SELECT SUM(jumlah_transaksi) FROM transaksi,kategori WHERE transaksi.id_kategori = kategori.id_kategori AND jenis_kategori = 'Pengeluaran' AND MONTH(tanggal_transaksi) = 3";
            PreparedStatement ps15 = new koneksi().getConnection().prepareStatement(sql15);
            ResultSet rs15 = ps15.executeQuery(sql15);
            if(rs15.next()){
               pengeluaranFeb = rs15.getInt(1);
            }
            
            int pengeluaranApr = 0;
            String sql16 = "SELECT SUM(jumlah_transaksi) FROM transaksi,kategori WHERE transaksi.id_kategori = kategori.id_kategori AND jenis_kategori = 'Pengeluaran' AND MONTH(tanggal_transaksi) = 4";
            PreparedStatement ps16 = new koneksi().getConnection().prepareStatement(sql16);
            ResultSet rs16 = ps16.executeQuery(sql16);
            if(rs16.next()){
               pengeluaranApr = rs16.getInt(1);
            }
            
            int pengeluaranMei = 0;
            String sql17 = "SELECT SUM(jumlah_transaksi) FROM transaksi,kategori WHERE transaksi.id_kategori = kategori.id_kategori AND jenis_kategori = 'Pengeluaran' AND MONTH(tanggal_transaksi) = 5";
            PreparedStatement ps17 = new koneksi().getConnection().prepareStatement(sql17);
            ResultSet rs17 = ps17.executeQuery(sql17);
            if(rs17.next()){
               pengeluaranMei = rs17.getInt(1);
            }
            
            int pengeluaranJun = 0;
            String sql18 = "SELECT SUM(jumlah_transaksi) FROM transaksi,kategori WHERE transaksi.id_kategori = kategori.id_kategori AND jenis_kategori = 'Pengeluaran' AND MONTH(tanggal_transaksi) = 6";
            PreparedStatement ps18 = new koneksi().getConnection().prepareStatement(sql18);
            ResultSet rs18 = ps18.executeQuery(sql18);
            if(rs18.next()){
               pengeluaranJun = rs18.getInt(1);
            }
            
            int pengeluaranJul = 0;
            String sql19 = "SELECT SUM(jumlah_transaksi) FROM transaksi,kategori WHERE transaksi.id_kategori = kategori.id_kategori AND jenis_kategori = 'Pengeluaran' AND MONTH(tanggal_transaksi) = 7";
            PreparedStatement ps19 = new koneksi().getConnection().prepareStatement(sql19);
            ResultSet rs19 = ps19.executeQuery(sql19);
            if(rs19.next()){
               pengeluaranJul = rs19.getInt(1);
            }
            
            int pengeluaranAgu = 0;
            String sql20 = "SELECT SUM(jumlah_transaksi) FROM transaksi,kategori WHERE transaksi.id_kategori = kategori.id_kategori AND jenis_kategori = 'Pengeluaran' AND MONTH(tanggal_transaksi) = 8";
            PreparedStatement ps20 = new koneksi().getConnection().prepareStatement(sql20);
            ResultSet rs20 = ps20.executeQuery(sql20);
            if(rs20.next()){
               pengeluaranAgu = rs20.getInt(1);
            }
            
            int pengeluaranSep = 0;
            String sql21 = "SELECT SUM(jumlah_transaksi) FROM transaksi,kategori WHERE transaksi.id_kategori = kategori.id_kategori AND jenis_kategori = 'Pengeluaran' AND MONTH(tanggal_transaksi) = 9";
            PreparedStatement ps21 = new koneksi().getConnection().prepareStatement(sql21);
            ResultSet rs21 = ps21.executeQuery(sql21);
            if(rs21.next()){
               pengeluaranSep = rs9.getInt(1);
            }
            
            int pengeluaranOkt = 0;
            String sql22 = "SELECT SUM(jumlah_transaksi) FROM transaksi,kategori WHERE transaksi.id_kategori = kategori.id_kategori AND jenis_kategori = 'Pengeluaran' AND MONTH(tanggal_transaksi) = 10";
            PreparedStatement ps22 = new koneksi().getConnection().prepareStatement(sql22);
            ResultSet rs22 = ps22.executeQuery(sql22);
            if(rs22.next()){
               pengeluaranOkt = rs22.getInt(1);
            }
            
            int pengeluaranNov = 0;
            String sql23 = "SELECT SUM(jumlah_transaksi) FROM transaksi,kategori WHERE transaksi.id_kategori = kategori.id_kategori AND jenis_kategori = 'Pengeluaran' AND MONTH(tanggal_transaksi) = 11";
            PreparedStatement ps23 = new koneksi().getConnection().prepareStatement(sql23);
            ResultSet rs23 = ps23.executeQuery(sql23);
            if(rs23.next()){
               pengeluaranNov = rs23.getInt(1);
            }
            
            int pengeluaranDes = 0;
            String sql24 = "SELECT SUM(jumlah_transaksi) FROM transaksi,kategori WHERE transaksi.id_kategori = kategori.id_kategori AND jenis_kategori = 'Pengeluaran' AND MONTH(tanggal_transaksi) = 12";
            PreparedStatement ps24 = new koneksi().getConnection().prepareStatement(sql24);
            ResultSet rs24 = ps24.executeQuery(sql24);
            if(rs24.next()){
               pengeluaranDes = rs24.getInt(1);
            }
            
            
            DefaultCategoryDataset dataset = new DefaultCategoryDataset();
            dataset.setValue(pemasukanJan, "Pemasukan", "Jan");
            dataset.setValue(pemasukanFeb, "Pemasukan", "Feb");
            dataset.setValue(pemasukanMar, "Pemasukan", "Mar");
            dataset.setValue(pemasukanApr, "Pemasukan", "Apr");
            dataset.setValue(pemasukanMei, "Pemasukan", "Mei");
            dataset.setValue(pemasukanJun, "Pemasukan", "Jun");
            dataset.setValue(pemasukanJul, "Pemasukan", "Jul");
            dataset.setValue(pemasukanAgu, "Pemasukan", "Agu");
            dataset.setValue(pemasukanSep, "Pemasukan", "Sep");
            dataset.setValue(pemasukanOkt, "Pemasukan", "Okt");
            dataset.setValue(pemasukanNov, "Pemasukan", "Nov");
            dataset.setValue(pemasukanDes, "Pemasukan", "Des");
            dataset.setValue(pengeluaranJan, "Pengeluaran", "Jan");
            dataset.setValue(pengeluaranFeb, "Pengeluaran", "Feb");
            dataset.setValue(pengeluaranMar, "Pengeluaran", "Mar");
            dataset.setValue(pengeluaranApr, "Pengeluaran", "Apr");
            dataset.setValue(pengeluaranMei, "Pengeluaran", "Mei");
            dataset.setValue(pengeluaranJun, "Pengeluaran", "Jun");
            dataset.setValue(pengeluaranJul, "Pengeluaran", "Jul");
            dataset.setValue(pengeluaranAgu, "Pengeluaran", "Agu");
            dataset.setValue(pengeluaranSep, "Pengeluaran", "Sep");
            dataset.setValue(pengeluaranOkt, "Pengeluaran", "Okt");
            dataset.setValue(pengeluaranNov, "Pengeluaran", "Nov");
            dataset.setValue(pengeluaranDes, "Pengeluaran", "Des");
            
            JFreeChart chart = ChartFactory.createBarChart3D("Rekapitulasi Tahunan", "Bulan", "", dataset);
            chart.setBackgroundPaint(Color.yellow);
            ChartFrame frame = new ChartFrame("Barchart", chart);
            frame.setVisible(true);
            frame.setSize(450,350);
        } catch (SQLException ex) {
            Logger.getLogger(MenuUtama.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
    }//GEN-LAST:event_jButton8ActionPerformed

    private void tabelPencarianMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tabelPencarianMouseClicked
        // TODO add your handling code here:
        edit.setEnabled(true);
        hapus.setEnabled(true);
    }//GEN-LAST:event_tabelPencarianMouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MenuUtama.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MenuUtama.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MenuUtama.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MenuUtama.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                try {
                    new MenuUtama().setVisible(true);
                } catch (SQLException ex) {
                    Logger.getLogger(MenuUtama.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel bulanan;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.ButtonGroup buttonGroup2;
    private javax.swing.JCheckBox cekKat;
    private javax.swing.JButton edit;
    private javax.swing.JButton hapus;
    private javax.swing.JPanel harian;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton10;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private javax.swing.JButton jButton7;
    private javax.swing.JButton jButton8;
    private javax.swing.JButton jButton9;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPopupMenu jPopupMenu1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private java.awt.TextField jumlahTransaksi;
    private javax.swing.JComboBox<String> kategori;
    private javax.swing.JComboBox<String> kategoriCari;
    private java.awt.Label label1;
    private java.awt.Label label10;
    private java.awt.Label label11;
    private java.awt.Label label2;
    private java.awt.Label label3;
    private java.awt.Label label4;
    private java.awt.Label label5;
    private java.awt.Label label6;
    private java.awt.Label label9;
    private javax.swing.JLabel nama;
    private java.awt.TextField namaTransaksi;
    private java.awt.TextField nama_transaksi;
    private javax.swing.JRadioButton pemasukan;
    private javax.swing.JLabel pemasukanBulanan;
    private javax.swing.JRadioButton pemasukanCari;
    private javax.swing.JLabel pemasukanHarian;
    private javax.swing.JLabel pemasukanTahunan;
    private javax.swing.JPanel pencarian;
    private javax.swing.JRadioButton pengeluaran;
    private javax.swing.JLabel pengeluaranBulanan;
    private javax.swing.JRadioButton pengeluaranCari;
    private javax.swing.JLabel pengeluaranHarian;
    private javax.swing.JLabel pengeluaranTahunan;
    private javax.swing.JComboBox<String> rekening;
    private java.awt.Label rekening1;
    private javax.swing.JComboBox<String> rekeningCari;
    private java.awt.Label saldo1;
    private javax.swing.JTable tabelBulanan;
    private javax.swing.JTable tabelHarian;
    private javax.swing.JTable tabelPencarian;
    private javax.swing.JTable tabelTahunan;
    private javax.swing.JPanel tahunan;
    private javax.swing.JLabel tanggalBesar;
    private javax.swing.JTabbedPane transaksiTab;
    // End of variables declaration//GEN-END:variables
}
